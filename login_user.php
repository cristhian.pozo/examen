<?php
session_start();
require_once './config/config.php';
//If User has already logged in, redirect to dashboard page.
if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] === TRUE) {
    header('Location:index.php');
}
include_once 'includes/header.php';
?>
<div id="page-" class="col-md-4 col-md-offset-4">
	<form class="form loginform" method="POST" action="authenticate_user.php">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Please Sign in</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="control-label">Name</label>
					<input type="text" name="name" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label class="control-label">Last name</label>
					<input type="text" name="last" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label class="control-label">Email</label>
					<input type="text" name="email" class="form-control" required="required">
				</div>
				
				
				<button type="submit" class="btn btn-success loginField" ><a href="login_user.php"></a>ingresar</button>
			</div>
		</div>
	</form>
</div>